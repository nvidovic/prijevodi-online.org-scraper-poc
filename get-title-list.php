<?php
/**
 * Created by PhpStorm.
 * User: madobor
 * Date: 3/9/14
 * Time: 4:46 PM
 */

function parse($id){

    /* GET key for download */
    $source = file_get_contents("http://www.prijevodi-online.org/serije/view/".$id."/");
    $tmp = explode("epizode.key", $source);
    $key = explode("'", $tmp[1]);
    $key = $key[1];

    /* Parse everything... */
    $complete = "";
    $dom = new DOMDocument;

    @$dom->loadHTML($source);
    $xpath = new DOMXPath($dom);
    $nodes = $xpath->query('//ul[@class="epizoda actual"]');
    foreach($nodes as $node) {
        foreach($node->getElementsByTagName('a') as $ahref) {
            $id_episode = $ahref->getAttribute('rel');
            $id_episode = preg_replace("/[^0-9]/","",$id_episode);
            $show_name =  strtolower($ahref->nodeValue);
            $complete = $id_episode."|".$show_name."<br>";
            if(strlen($complete) >= "11") { // little workaround for DOM
                //if (strpos($complete,$series_episode_name) !== false) {
                    generateList($key,$id_episode);
                //}
            }
        }

    }
}

function generateList($key, $id_episode){

    //set POST variables
    $url = "http://www.prijevodi-online.org/prijevod/get/".$id_episode."/";
    $post = "key=".$key;

    //open connection
    $ch = curl_init();

    //set the url, number of POST vars, POST data
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POST, "1");
    curl_setopt($ch,CURLOPT_POSTFIELDS, $post);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);

    $DOM = new DOMDocument();
    $DOM->loadHTML($result);
    $a = $DOM->getElementsByTagName('a');
    foreach($a as $link){
        $url = $link->getAttribute('href');
        $name = $link->nodeValue;
        echo "<a href=download-proxy.php?url=".$url.">".$name."</a></br>";

    }
}

function getId($series_name){
    $con=mysqli_connect("localhost","root","root","prijevodi-online", 8889);
    if (mysqli_connect_errno())
    {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }

    $sql = "SELECT id FROM popis WHERE naziv LIKE '$series_name'";
    $res = mysqli_query($con,$sql);
    while($row = mysqli_fetch_array($res))
    {
        return $row['id'];
    }
}


$series_name = htmlspecialchars($_GET["series_name"]);
//$series_episode_name = strtolower(htmlspecialchars($_GET["series_episode_name"]));
$id = getId($series_name);

if($id === null) {
    echo "No results.";
}
else {
    $key = parse($id);

}

?>